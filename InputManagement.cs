﻿using System;
using System.Collections.Generic;
using System.Threading;
using InputManagerCS;
using System.Windows.Forms;

namespace HeadLightFlash
{
    public class InputManagement
    {
        public int KeyDownTime { get; private set; }

        private const int delay = 8;


        public InputManagement()
        {
            KeyDownTime = 20;


            try
            {
                PressButton(Keys.Control);
            }
            catch
            {
                KeyDownTime = -1; //ErrorCode
                SimHub.Logging.Current.Error("HeadlightFlasher: Error with Keyboard Emulator, keypress test failed, Plugin deactivated");
            }

        }

        public void PressButton(Keys key)
        {
            try
            {
                Keyboard.KeyDown(key);
                Thread.Sleep(KeyDownTime - delay);
                Keyboard.KeyUp(key);
                Thread.Sleep(delay); //making sure the keyup promt was correctly recieved
            }
            catch
            {
                SimHub.Logging.Current.Error("HeadlightFlasher: Failed to Press Key");
            }

        }

        public static Keys ConvertToVirtualKeyCode(System.Windows.Input.Key key)
        {
            int value = (int)key;
            Keys keyCode = Keys.L;

            if (value == 2 || value == 3)
            {
                keyCode = (Keys)value + 6;
            }
            else if (value == 6)
            {
                keyCode = Keys.Enter;
            }
            else if (value == 7)
            {
                keyCode = (Keys)value + 12;
            }
            else if (value == 8)
            {
                if (key == System.Windows.Input.Key.Capital)
                    keyCode = Keys.Capital;
                else
                    keyCode = Keys.CapsLock;
            }
            else if (value >= 13 && value < 44) //Numbers, and a bunch of others
            {
                keyCode = (Keys)value + 14;
            }
            else if (value >= 44 && value < 73) //Letters mainly
            {
                keyCode = (Keys)value + 21;
            }
            else if (value >= 73 && value < 114) //NumPad + F1 to F24
            {
                keyCode = (Keys)value + 22;
            }
            else if (value == 114 || value == 115) //NumLock + ScrollLock
            {
                keyCode = (Keys)value + 30;
            }
            else if (value >= 116 && value < 140) //Shift, Control, Browser and Media Control
            {
                keyCode = (Keys)value + 44;
            }
            else if (value >= 140 && value < 147) //OEM
            {
                keyCode = (Keys)value + 46;
            }
            else if (value >= 149 && value < 153) //OEM
            {
                keyCode = (Keys)value + 70;
            }
            else if (value == 154)
            {
                if (key == System.Windows.Input.Key.Oem102)
                    keyCode = Keys.Oem102;
                else
                    keyCode = Keys.OemBackslash;
            }
            else
            {
                SimHub.Logging.Current.Error("HeadlightFlasher: Could not convert key " + key.ToString() + " into a Headlight key to press. Pressing L instead. Please remap the key");
            }

            return keyCode;
        }

        /// <summary>
        /// This converts a message into a int array of the intervals till the next switching.
        /// So for "SOS" it returns [1,1,1,1,1,3,3,1,3,1,3,3,1,1,1,1,1].
        /// Wait time between letters is 3 units, 7 units between words, 1 unit between signals, short is 1 unit, long is 3 units.
        /// 
        /// Usage by iterating over the array with a loop like this: doing keystroke (or other signal activation), multiplying the value out of the array with your unit length and waiting for that amount of time
        /// </summary>
        /// <param name="text">text to be converted. Only letters (case insensitive), spaces and numbers are accepted, new lines will be turned into 2 spaces, tabs into 4 spaces, everything else is dropped</param>
        /// <returns></returns>
        public static int[] ConvertToMorse(string text)
        {
            List<int> list = new List<int>();

            text = text.Trim().ToUpper().Replace("\n", "  ").Replace("\t", "    ");
            char[] arr = text.ToCharArray();

            for (int i = 0; i < arr.Length; i++)
            {
                bool skip = false;

                switch (arr[i])
                {
                    case 'A': list.AddRange(new int[] { 1, 1, 3 }); break;
                    case 'B': list.AddRange(new int[] { 3, 1, 1, 1, 1, 1, 1 }); break;
                    case 'C': list.AddRange(new int[] { 3, 1, 1, 1, 3, 1, 1 }); break;
                    case 'D': list.AddRange(new int[] { 3, 1, 1, 1, 1 }); break;
                    case 'E': list.AddRange(new int[] { 1 }); break;
                    case 'F': list.AddRange(new int[] { 1, 1, 1, 1, 3, 1, 1 }); break;
                    case 'G': list.AddRange(new int[] { 3, 1, 3, 1, 1 }); break;
                    case 'H': list.AddRange(new int[] { 1, 1, 1, 1, 1, 1, 1 }); break;
                    case 'I': list.AddRange(new int[] { 1, 1, 1}); break;
                    case 'J': list.AddRange(new int[] { 1, 1, 3, 1, 3, 1, 3 }); break;
                    case 'K': list.AddRange(new int[] { 3, 1, 1, 1, 3 }); break;
                    case 'L': list.AddRange(new int[] { 1, 1, 3, 1, 1, 1, 1 }); break;
                    case 'M': list.AddRange(new int[] { 3, 1, 3 }); break;
                    case 'N': list.AddRange(new int[] { 3, 1, 1 }); break;
                    case 'O': list.AddRange(new int[] { 3, 1, 3, 1, 3 }); break;
                    case 'P': list.AddRange(new int[] { 1, 1, 3, 1, 3, 1, 1 }); break;
                    case 'Q': list.AddRange(new int[] { 3, 1, 3, 1, 1, 1, 3 }); break;
                    case 'R': list.AddRange(new int[] { 1, 1, 3, 1, 1 }); break;
                    case 'S': list.AddRange(new int[] { 1, 1, 1, 1, 1 }); break;
                    case 'T': list.AddRange(new int[] { 3 }); break;
                    case 'U': list.AddRange(new int[] { 1, 1, 1, 1, 3 }); break;
                    case 'V': list.AddRange(new int[] { 1, 1, 1, 1, 1, 1, 3 }); break;
                    case 'W': list.AddRange(new int[] { 1, 1, 3, 1, 3 }); break;
                    case 'X': list.AddRange(new int[] { 3, 1, 1, 1, 1, 1, 3 }); break;
                    case 'Y': list.AddRange(new int[] { 3, 1, 1, 1, 3, 1, 3 }); break;
                    case 'Z': list.AddRange(new int[] { 3, 1, 3, 1, 1, 1, 1 }); break;
                    case '1': list.AddRange(new int[] { 1, 1, 3, 1, 3, 1, 3, 1, 3 }); break;
                    case '2': list.AddRange(new int[] { 1, 1, 1, 1, 3, 1, 3, 1, 3 }); break;
                    case '3': list.AddRange(new int[] { 1, 1, 1, 1, 1, 1, 3, 1, 3 }); break;
                    case '4': list.AddRange(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 3 }); break;
                    case '5': list.AddRange(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 }); break;
                    case '6': list.AddRange(new int[] { 3, 1, 1, 1, 1, 1, 1, 1, 1 }); break;
                    case '7': list.AddRange(new int[] { 3, 1, 3, 1, 1, 1, 1, 1, 1 }); break;
                    case '8': list.AddRange(new int[] { 3, 1, 3, 1, 3, 1, 1, 1, 1 }); break;
                    case '9': list.AddRange(new int[] { 3, 1, 3, 1, 3, 1, 3, 1, 1 }); break;
                    case '0': list.AddRange(new int[] { 3, 1, 3, 1, 3, 1, 3, 1, 3 }); break;
                    case ' ': list.Add(7); skip = true; break;
                    default: skip = true; break;
                }

                if (!skip && (i + 1) < arr.Length)
                {
                    if (arr[i + 1] == ' ')
                    {
                        list.Add(7);
                        i++; //skipping the space, as we already added it here
                    }
                    else //just normal letter spacing
                    {
                        list.Add(3); 
                    }
                }
            }

            return list.ToArray();
        }
    }
}
