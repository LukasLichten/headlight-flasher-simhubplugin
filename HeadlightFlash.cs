﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Windows.Forms;
using System.Windows.Input;

namespace HeadLightFlash
{



    [PluginDescription("Adds a configurable Headlight Flasher")]
    [PluginAuthor("Lukas Lichten")]
    [PluginName("Headlight Flasher")]
    public class HeadlightFlash : IPlugin, IWPFSettings
    {

        public HeadlightFlashSettings Settings { get; set; }


        /// <summary>
        /// Instance of the current plugin manager
        /// </summary>
        public PluginManager PluginManager { get; set; }

        public InputManagement InputManagement { get; set; }

        private TimeSpan LastFlash { get; set; } //technically the predicted end time of the last flash

        /// <summary>
        /// Called at plugin manager stop, close/dispose anything needed here ! 
        /// Plugins are rebuilt at game change
        /// </summary>
        /// <param name="pluginManager"></param>
        public void End(PluginManager pluginManager)
        {
            // Save settings
            this.SaveCommonSettings("HeadlightFlash", Settings);
        }

        /// <summary>
        /// Returns the settings control, return null if no settings control is required
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <returns></returns>
        public System.Windows.Controls.Control GetWPFSettingsControl(PluginManager pluginManager)
        {
            return new HeadlightFlashSettingsControll(this);
        }

        /// <summary>
        /// Called once after plugins startup
        /// Plugins are rebuilt at game change
        /// </summary>
        /// <param name="pluginManager"></param>
        public void Init(PluginManager pluginManager)
        {
            HeadlightFlashSettings defaultValues = new HeadlightFlashSettings()
            { SettingsContainerVersion = 2 , HeadlightKeyCode = Key.L, FlashLength = 200, NumberOfFlashes = 2, IsEnabled = true, MorseUnitLength = 75, MorseText = "", MorseIsEnabled = false, Rf2TrippleMode = true };
            
            // Load settings
            Settings = this.ReadCommonSettings<HeadlightFlashSettings>("HeadlightFlash", () => defaultValues);
            
            if (Settings.SettingsContainerVersion != defaultValues.SettingsContainerVersion)
                UpdateSettings();

            InputManagement = new InputManagement();

            LastFlash = new TimeSpan(0);

            // Declare an event 
            //pluginManager.AddEvent("", this.GetType());

            // Declare an action which can be called
            pluginManager.AddAction("FlashHeadlights", this.GetType(), FlashHeadlights);
            pluginManager.AddAction("ToggleHeadlights", this.GetType(), SwitchHeadlights);

            pluginManager.AddAction("FlashMessage", this.GetType(), FlashMessage);
        }

        private void UpdateSettings()
        {
            if (Settings.SettingsContainerVersion < 1) //v1.0 of the software, have to add Morse and rf2 tripple
            {
                Settings.SettingsContainerVersion = 2;
                Settings.MorseUnitLength = 75;
                Settings.MorseText = "";
                Settings.MorseIsEnabled = false;
                Settings.Rf2TrippleMode = true;
            }
        }

        public void FlashHeadlights(PluginManager pluginManager, string para)
        {
            TimeSpan now = DateTime.Now.Subtract(new DateTime(1970, 1, 1));

            if (!Settings.IsEnabled || !(now > LastFlash) || InputManagement.KeyDownTime == -1)
                return;

            //Getting to the actual flashing
            int totalLength = (Settings.FlashLength * Settings.NumberOfFlashes * 2) + 200;
            LastFlash = now.Add(new TimeSpan(0, 0, 0, 0, totalLength));
            var input = InputManagement.ConvertToVirtualKeyCode(Settings.HeadlightKeyCode);

            int rf2Mode = -1;
            if (pluginManager.GameName == "RFactor2" && Settings.Rf2TrippleMode)
                rf2Mode = 0;

            for (int i = 0; i < Settings.NumberOfFlashes; i++)
            {
                if (rf2Mode == -1)
                    NormalFlash(input);
                else
                    rf2Mode = Rf2Flash(rf2Mode, pluginManager, input);
            }
        }

        public void SwitchHeadlights(PluginManager pluginManager, string para)
        {
            if (InputManagement.KeyDownTime == -1)
                return;

            var input = InputManagement.ConvertToVirtualKeyCode(Settings.HeadlightKeyCode);
            InputManagement.PressButton(input);

        }

        public void FlashMessage(PluginManager pluginManager, string para)
        {
            TimeSpan now = DateTime.Now.Subtract(new DateTime(1970, 1, 1));

            if (!Settings.IsEnabled || !Settings.MorseIsEnabled || !(now > LastFlash) || InputManagement.KeyDownTime == -1)
                return;

            int[] morseCode = InputManagement.ConvertToMorse(Settings.MorseText);

            //Stops another flash being started before this one has finished
            int totalLength = 200;
            foreach (var item in morseCode)
            {
                totalLength += item * Settings.MorseUnitLength;
            }
            LastFlash = now.Add(new TimeSpan(0, 0, 0, 0, totalLength));

            var input = InputManagement.ConvertToVirtualKeyCode(Settings.HeadlightKeyCode);

            int rf2Mode = -1;
            if (pluginManager.GameName == "RFactor2" && Settings.Rf2TrippleMode)
                rf2Mode = 0;

            bool rf2State = false; //used with rf2Mode to determine if there is a need for 2 or 1 press to switch

            //Flashing
            foreach (var item in morseCode)
            {
                int time = (Settings.MorseUnitLength * item);

                if (rf2Mode == -1)
                {
                    InputManagement.PressButton(input);
                    System.Threading.Thread.Sleep(time - InputManagement.KeyDownTime);
                }
                else
                {
                    rf2Mode = SwitchRf2Lights(rf2Mode, pluginManager, input, time, rf2State);
                    rf2State = !rf2State;
                }
            }

            if (rf2Mode == -1)
            {
                InputManagement.PressButton(input);
            }
            else
            {
                rf2Mode = SwitchRf2Lights(rf2Mode, pluginManager, input, 0, rf2State);
            }
            
        }

        private void NormalFlash(Keys input)
        {
            InputManagement.PressButton(input);
            System.Threading.Thread.Sleep(Settings.FlashLength - InputManagement.KeyDownTime);
            InputManagement.PressButton(input);
            System.Threading.Thread.Sleep(Settings.FlashLength - InputManagement.KeyDownTime);
        }

        //RFactor 2 mode:
        //Headlights go through Off-Auto-On
        //Four start out conditions possible:
        //-Off, Headlights Off. 2 Presses (->Auto->On), wait, 1 Press (->Off)
        //-Auto, Headlights Off, 1 Press (->On), wait, 2 Presses (->Off->Auto)
        //-Auto, Headlights On, 2 Presses (->On->Off), wait, 1 Press (->Auto)
        //-On, Headlights On, 1 Press (->Off), wait, 2 Presses (->Auto->On)
        //
        //Headlight state is known, mode state is not
        //Problem: While with Headligt state it can correctly switch, it is only for States Off/On OR the two Auto States, not all four.
        //The code could be written for the assumption that the user uses Auto, but if he does not, everything breaks

        /// <summary>
        /// This deals with the new Auto Headlight mode for rf2, which makes the headlight button from a off-on toggle to a off-auto-on toggle.
        /// This creates multiple unique situations, where either the headlight button has to be pressed twice or only once to switch them.
        /// This function deals with this problem
        /// Does three presses and the appropriate waits to complete a Flash
        /// </summary>
        /// <param name="mode">If this is 0, it checks the mode, with 1, 2 and 3 it does the corresponding sequence</param>
        /// <param name="pluginManager">The Pluginmanager, required to find out the mode</param>
        /// <param name="input">The Key to press</param>
        /// <returns>The mode, pass this into the next run of this function</returns>
        private int Rf2Flash(int mode, PluginManager pluginManager, Keys input)
        {
            mode = SwitchRf2Lights(mode, pluginManager, input, Settings.FlashLength, false);
            mode = SwitchRf2Lights(mode, pluginManager, input, Settings.FlashLength, true);
            return mode;
        }

        /// <summary>
        /// This deals with the new Auto Headlight mode for rf2, which makes the headlight button from a off-on toggle to a off-auto-on toggle.
        /// This creates multiple unique situations, where either the headlight button has to be pressed twice or only once to switch them.
        /// This function deals with this problem
        /// This switched the headlights from a state of On to one of off. This is however not to be treated as a toggle, it is to run different lengths for the interval (when for example sending Morse)
        /// </summary>
        /// <param name="mode">If this is 0, it checks the mode, with 1, 2 and 3 it does the corresponding sequence</param>
        /// <param name="pluginManager">The Pluginmanager, required to find out the mode</param>
        /// <param name="input">The Key to press</param>
        /// <param name="time">time to wait. The keydown time will be subtracted in this function</param>
        /// <param name="selectHalf">this is not a switch on/off, on first run pass false, on second run pass true. If mode=0, this variable is ignored and effectivly false is executed</param>
        /// <returns>The mode, pass this into the next run of this function</returns>
        public int SwitchRf2Lights(int mode, PluginManager pluginManager, Keys input, int time, bool selectHalf)
        {
            if (mode == 0)
            {
                //Determin the mode
                int startValue = Int32.Parse(pluginManager.GetPropertyValue("DataCorePlugin.GameRawData.CurrentPlayer.mHeadlights").ToString());
                int startUpdate = Int32.Parse(pluginManager.GetPropertyValue("DataCorePlugin.GameRawData.extended.mVersionUpdateBegin").ToString());

                InputManagement.PressButton(input);
                int downtime = 0;

                int changeUpdate;

                do
                {
                    changeUpdate = Int32.Parse(pluginManager.GetPropertyValue("DataCorePlugin.GameRawData.extended.mVersionUpdateBegin").ToString());
                    System.Threading.Thread.Sleep(10);
                    downtime += 10;
                } while (changeUpdate == startUpdate && downtime < 2000); //insure new data update has arrived, but also insure the software does not softlock if no update arrives ever

                int changeValue = Int32.Parse(pluginManager.GetPropertyValue("DataCorePlugin.GameRawData.CurrentPlayer.mHeadlights").ToString());

                if (startValue == changeValue && startValue == 1) //Special case: Headlight where in Auto, but on. Autoheadlights is too slow in turning on, so we have to cycle to on for our on portion
                {
                    mode = 3;
                    if (time > downtime)
                        time -= downtime; //To compensate for the longer delay
                    else
                        time = 0;

                }
                else if (startValue == changeValue)
                {
                    mode = 2;
                    InputManagement.PressButton(input);

                    //Time wasted between the first two presses is just time wasted, we won't make up for it
                }
                else if (startValue != changeValue)
                {
                    mode = 1;
                    if (time > downtime)
                        time -= downtime; //To compensate for the longer delay
                    else
                        time = 0;

                    //If it took longer then the flash anyway we just don't sleep to not waste any further time
                }
            }
            else if (mode == 1)
            {
                //Start point is either Auto (Off), Off (going to Auto (On)), or On
                //Presses once, waits, then presses twice
                if (!selectHalf)
                {
                    InputManagement.PressButton(input);
                }
                else
                {
                    InputManagement.PressButton(input);
                    InputManagement.PressButton(input);
                }
            }
            else if (mode == 2)
            {
                //Start point is Off (going through Auto (Off))
                //Presses twice, waits, then presses once
                if (!selectHalf)
                {
                    InputManagement.PressButton(input);
                    InputManagement.PressButton(input);
                }
                else
                {
                    InputManagement.PressButton(input);
                }
            }
            else if (mode == 3)
            {
                //Special case: Auto (On) is the start case, but Auto headlight are too slow for turning on
                //so we cycle to On, wait there, cycle to off, wait there, and then return to Auto
                if (!selectHalf)
                {
                    InputManagement.PressButton(input);
                }
                else
                {
                    InputManagement.PressButton(input);
                    if (time > InputManagement.KeyDownTime)
                        System.Threading.Thread.Sleep(time - InputManagement.KeyDownTime);
                    InputManagement.PressButton(input);

                    return mode; //we in this case wait in between these two presses, rather then after, so we exit here to avoid the general wait
                }
            }

            if (time > InputManagement.KeyDownTime)
                System.Threading.Thread.Sleep(time - InputManagement.KeyDownTime);

            return mode;
        }
    }
}