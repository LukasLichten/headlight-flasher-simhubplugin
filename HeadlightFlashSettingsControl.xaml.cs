﻿using Ownskit.Utils;
using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Security.Principal;

namespace HeadLightFlash
{
    /// <summary>
    /// Control Panel
    /// </summary>
    public partial class HeadlightFlashSettingsControll : UserControl
    {
        public HeadlightFlash Plugin { get; private set; }
        private Key KeyCode { get; set; }

        private RawKeyEventHandler KeyEvent;

        private KeyboardListener Listener;

        public HeadlightFlashSettingsControll(HeadlightFlash plugin)
        {
            InitializeComponent();

            this.Plugin = plugin;

            LoadSettings();
            BtnApply.Content = "Applied";

            //Done to prevent a bug where if not running with Admin perm it does not correctly register the keystroke, and the button would get stuck
            bool isElevated = false;
            using (WindowsIdentity identity = WindowsIdentity.GetCurrent())
            {
                WindowsPrincipal principal = new WindowsPrincipal(identity);
                isElevated = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            if (!isElevated)
            {
                BtnHeadlightKey.IsEnabled = false;
            }

            if (Plugin.InputManagement.KeyDownTime == -1)
            {
                this.IsEnabled = false;
                LbWarningBox.Content = "Plugin Disabled, Keyboard Emulation test failed. \nMost likely this version of SimHub is not compatible with this version of this Plugin any further.";
            }
        }

        private void LoadSettings()
        {
            SetFlashLength(Plugin.Settings.FlashLength);
            SetFlashNumber(Plugin.Settings.NumberOfFlashes);
            ChkRf2Mode.IsChecked = Plugin.Settings.Rf2TrippleMode;
            ChkEnabled.IsChecked = Plugin.Settings.IsEnabled;
            SetHeadlightButton(Plugin.Settings.HeadlightKeyCode);

            SetMorseLength(Plugin.Settings.MorseUnitLength);
            TbMorseText.Text = Plugin.Settings.MorseText;
            ChkMorseEnabled.IsChecked = Plugin.Settings.MorseIsEnabled;
            EnableMorse(Plugin.Settings.MorseIsEnabled);

            
            BtnApply.IsEnabled = false;
            BtnRevert.IsEnabled = false;

            UpdateWarningBox();
        }

        private void SetFlashLength(int length)
        {
            SlFlashLength.Value = length;
            TbFlashLength.Text = ""+length;

            TbFlashLength.Foreground = System.Windows.Media.Brushes.Black;

            Tweaks();
        }

        private void SlFlashLength_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            SetFlashLength((int)SlFlashLength.Value);
        }

        private void TbFlashLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            string text = TbFlashLength.Text;

            try
            {
                SetFlashLength(Int32.Parse(text));
            }
            catch
            {
                TbFlashLength.Foreground = System.Windows.Media.Brushes.Red;
                BtnApply.IsEnabled = false;
            }
        }

        private void SetFlashNumber(int num)
        {
            SlFlashNumber.Value = num;
            TbFlashNumber.Text = "" + num;

            TbFlashNumber.Foreground = System.Windows.Media.Brushes.Black;

            Tweaks();
        }

        private void SlFlashNumber_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            SetFlashNumber((int)SlFlashNumber.Value);
        }

        private void TbFlashNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            string text = TbFlashNumber.Text;

            try
            {
                SetFlashNumber(Int32.Parse(text));
            }
            catch
            {
                TbFlashNumber.Foreground = System.Windows.Media.Brushes.Red;
                BtnApply.IsEnabled = false;
            }
        }

        private void BtnHeadlightKey_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Listener = new KeyboardListener();
            KeyEvent = new RawKeyEventHandler(HeadlightButtonMap);

            Listener.KeyDown += KeyEvent;

            BtnHeadlightKey.Content = "Press Key (Esc to cancel)";
            BtnHeadlightKey.IsEnabled = false;
        }

        private void HeadlightButtonMap(object sender, RawKeyEventArgs args)
        {
            BtnHeadlightKey.IsEnabled = true;
            Listener.KeyDown -= KeyEvent;
            Listener.Dispose();

            //Cancel
            if (args.Key == Key.Escape)
            {
                SetHeadlightButton(KeyCode);
            }
            else
            {
                //Map
                SetHeadlightButton(args.Key);
                Tweaks();
            }
        }

        private void SetHeadlightButton(Key key)
        {
            KeyCode = key;
            BtnHeadlightKey.Content = KeyCode.ToString();
        }

        private void BtnApply_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            try
            {
                Plugin.Settings.FlashLength = Int32.Parse(TbFlashLength.Text);
                Plugin.Settings.NumberOfFlashes = Int32.Parse(TbFlashNumber.Text);
                Plugin.Settings.MorseUnitLength = Int32.Parse(TbMorseLength.Text);
            }
            catch
            {
                LbWarningBox.Content = "Write valid numbers into the text boxes";
                return;
            }

            int minimumTime = Plugin.InputManagement.KeyDownTime;
            
            Plugin.Settings.HeadlightKeyCode = KeyCode;
            Plugin.Settings.Rf2TrippleMode = ChkRf2Mode.IsChecked.Value;
            Plugin.Settings.IsEnabled = ChkEnabled.IsChecked.Value;

            Plugin.Settings.MorseText = TbMorseText.Text.ToUpper().Trim();
            TbMorseText.Text = Plugin.Settings.MorseText;
            Plugin.Settings.MorseIsEnabled = ChkMorseEnabled.IsChecked.Value;

            if (Plugin.Settings.FlashLength < minimumTime)
            {
                Plugin.Settings.FlashLength = minimumTime;
                SetFlashLength(minimumTime);
            }
            if (Plugin.Settings.NumberOfFlashes < 1)
            {
                Plugin.Settings.NumberOfFlashes = 1;
                SetFlashNumber(1);
            }
            if (Plugin.Settings.MorseUnitLength < minimumTime)
            {
                Plugin.Settings.MorseUnitLength = minimumTime;
                SetMorseLength(minimumTime);
            }

            BtnApply.Content = "Applied";
            BtnApply.IsEnabled = false;
            BtnRevert.IsEnabled = false;

            UpdateWarningBox();
        }

        private void Tweaks()
        {
            BtnApply.IsEnabled = true;
            BtnApply.Content = "Apply";
            BtnRevert.IsEnabled = true;
        }

        private void UpdateWarningBox()
        {
            string text = "";

            if (Plugin.Settings.NumberOfFlashes > 4 || (Plugin.Settings.NumberOfFlashes * Plugin.Settings.FlashLength * 2) > 2000 || Plugin.Settings.FlashLength < 100)
            {
                text += "\nThese settings might be regarded in online racing as excessive. Use at own risk";
            }

            /*
            if (Plugin.Settings.NumberOfFlashes > 4)
            {
                text += "\nSome Leagues restrict number of Flashes to 4.\nEven if no specific number is given, excessive flashing is usually not welcome. Use at own risk";
            }
            if ((Plugin.Settings.NumberOfFlashes * Plugin.Settings.FlashLength * 2) > 2000)
            {
                text += "\nSome Leagues restrict the total length to 2s.\nEven if no specific number is given, excessive flashing is usually not welcome. Use at own risk";
            }
            if (Plugin.Settings.FlashLength < 100)
            {
                text += "\nWith such a short Flashlength the flashes can be viewed as excessive. Use at own risk";
            }*/

            if (Plugin.Settings.MorseIsEnabled)
            {
                text += "\nUsage of the Message Flash is not suited for online racing (Can be deemed excessive)";
            }

            //Post processing
            if (!text.Equals(""))
            {
                text = text.Substring(1);
            }

            LbWarningBox.Content = text;
        }

        private void ChkEnabled_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            Tweaks();
        }

        private void SetMorseLength(int num)
        {
            SlMorseLength.Value = num;
            TbMorseLength.Text = "" + num;

            TbMorseLength.Foreground = System.Windows.Media.Brushes.Black;

            Tweaks();
        }

        private void SlMorseLength_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            SetMorseLength((int)SlMorseLength.Value);
        }

        private void TbMorseLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            string text = TbMorseLength.Text;

            try
            {
                SetMorseLength(Int32.Parse(text));
            }
            catch
            {
                TbMorseLength.Foreground = System.Windows.Media.Brushes.Red;
                BtnApply.IsEnabled = false;
            }
        }

        private void TbMorseText_TextChanged(object sender, TextChangedEventArgs e)
        {
            string text = TbMorseText.Text;
            text = text.ToUpper();

            bool isCorrect = true;

            foreach (var item in text)
            {
                if (!(('A' <= item && item <= 'Z') || ('0' <= item && item <= '9') || (item == ' ')))
                {
                    isCorrect = false;
                }
            }

            if (isCorrect)
            {
                TbMorseText.Foreground = System.Windows.Media.Brushes.Black;
                Tweaks();
            }
            else
            {
                TbMorseText.Foreground = System.Windows.Media.Brushes.Red;
                BtnApply.IsEnabled = false;
            }
        }

        private void EnableMorse(bool value)
        {
            TbMorseLength.IsEnabled = value;
            SlMorseLength.IsEnabled = value;
            TbMorseText.IsEnabled = value;
        }

        private void ChkMorseEnabled_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            EnableMorse(ChkMorseEnabled.IsChecked.Value);

            

            Tweaks();
        }

        private void BtnRevert_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            LoadSettings();

            BtnApply.Content = "Reset";
        }

        private void ChkRf2Mode_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            Tweaks();
        }
    }
}
