﻿using System.Windows.Input;

namespace HeadLightFlash
{
    /// <summary>
    /// Settings class, make sure it can be correctly serialized using JSON.net
    /// </summary>
    public class HeadlightFlashSettings
    {
        public int SettingsContainerVersion { get; set; } //This is to deal with updates that add settings, so users get the defaults for those newly added
        public Key HeadlightKeyCode { get; set; }
        public int FlashLength { get; set; }
        public int NumberOfFlashes { get; set; }
        public bool IsEnabled { get; set; }
        public string MorseText { get; set; }
        public int MorseUnitLength { get; set; }
        public bool MorseIsEnabled { get; set; }
        public bool Rf2TrippleMode { get; set; }
    }
}